<?php


$router->get('/', 'AllController@home');
$router->get('privacy-policy', ['as' => 'privacy-policy', 'uses' => 'AllController@privacyPolicy']);
$router->get('terms-and-conditions', ['as' => 'terms-and-conditions', 'uses' => 'AllController@termsAndConditions']);
$router->get('refund-policy', ['as' => 'refund', 'uses' => 'AllController@refundPolicy']);
$router->get('contact-us', ['as' => 'contact-us', 'uses' => 'AllController@contactUs']);
$router->post('contact-us', ['as' => 'contact-us-post', 'uses' => 'AllController@postContactUs']);
