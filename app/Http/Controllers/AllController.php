<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AllController extends Controller
{
    public function home(Request $request)
    {
        return view('home.index');
    }
    public function privacyPolicy(Request $request)
    {
        return view('privacy-policy.index');
    }
    public function refundPolicy(Request $request)
    {
        return view('refund-policy.index');
    }
    public function termsAndConditions(Request $request)
    {
        return view('terms-and-conditions.index');
    }
    public function contactUs(Request $request)
    {
        return view('contact-us.index');
    }
}
