@extends('layout.app') @section('content')

<main>
    <!-- Hero -->

    <section class="section sonew-header">
        <div class="sonew-hero sonew-hero-background text-white">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 col-sm-12 justify-content-center">
                    <img src="https://chois-common.s3.ap-south-1.amazonaws.com/assets/chois_home_baner.jpg" alt="Mobile App Mockup">
                </div>
                <div class="col-12 col-md-6 col-sm-12 text-left p-lg-7 p-5 ">
                    <h1 class="display-2 font-weight-bolder mb-4">
                        Ride Surge Free.
                    </h1>
                    <p class="lead mb-4 mb-lg-5">With CHOIS, you’ll never worry about surge pricing again. Be it heavy rain or traffic, we offer predictable pricing. Go Surge Free.</p>
                    <div>
                        <a href="#" class="btn btn-dark btn-download-app mb-xl-0 mr-2 mr-md-3 d-none">
                            <span class="d-flex align-items-center">
                                <span class="icon icon-brand mr-2 mr-md-3"><span class="fab fa-apple"></span></span>
                                <span class="d-inline-block text-left">
                                    <small class="font-weight-normal d-none d-md-block">Available on</small> App Store
                                </span>
                            </span>
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.rideplus.customer" target="_blank" class="btn btn-download-app btn-sonew text-white">
                            <span class="d-flex align-items-center">
                                <span class="icon icon-brand mr-2 mr-md-3"><span class="fab fa-google-play"></span></span>
                                <span class="d-inline-block text-left ">
                                    <small class="font-weight-normal d-none d-md-block">User App</small>
                                    <small class="font-weight-normal d-none d-md-block">Available on</small> Google Play
                                </span>
                            </span>
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.rideplus.partner" target="_blank" class="btn btn-download-app btn-sonew text-white">
                            <span class="d-flex align-items-center">
                                <span class="icon icon-brand mr-2 mr-md-3"><span class="fab fa-google-play"></span></span>
                                <span class="d-inline-block text-left ">
                                    <small class="font-weight-normal d-none d-md-block">Parter App</small>
                                    <small class="font-weight-normal d-none d-md-block">Available on</small> Google Play
                                </span>
                            </span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>


    <section class="section section-lg py-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <div class="card border-0 bg-white text-center p-1">
                        <div class="card-header bg-white border-0 pb-0">
                            <img class="" src="./assets/img/11.png" alt="Mobile App Mockup">
                            <h2 class="h3 text-dark m-0">Reliable Cabs</h2>
                        </div>
                        <div class="card-body">
                            <p>
                                We provide you with best in class cabs for your best travel experience.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <div class="card border-0 bg-white text-center p-1">
                        <div class="card-header bg-white border-0 pb-0">
                            <img class="" src="./assets/img/22.png" alt="Mobile App Mockup">
                            <h2 class="h3 text-dark m-0">Ontime Service</h2>
                        </div>
                        <div class="card-body">
                            <p>
                                Get cabs at your door step ontime everytime.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card border-0 bg-white text-center p-1">
                        <div class="card-header bg-white border-0 pb-0">
                            <img class="" src="./assets/img/33.png" alt="Mobile App Mockup">
                            <h2 class="h3 text-dark m-0">Professional Drivers</h2>
                        </div>
                        <div class="card-body">
                            <p>
                                Our well trained and higly professional drivers will make your riding experience pleasurable.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-lg" id="about">
        <div class="row justify-content-center mb-5 mb-lg-7 py-4 py-md-6" style="background:#19BE72 ;">
            <div class="col-8 col-md-4 col-lg-8 text-center text-white">
                <h2 class="h1 mb-4">Better in every way</h2>
                <p class="lead">With CHOIS, you’ll never worry about surge pricing again. Be it heavy rain or traffic, we offer predictable pricing. Go Surge Free.</p>
            </div>
        </div>
        <div class="container">
            <div class="row row-grid align-items-center mb-5 mb-lg-7">
                <div class="col-12 col-lg-5">
                    <h2 class="mb-4">Revolution in cab industry</h2>
                    <p>The world of cab services is about to change, and you deserve to be a part of it. Install our easy to use app and put your time and driving skills to good use, without being undervalued. </p>
                </div>
                <div class="col-12 col-lg-6 ml-lg-auto">
                    <img src="./assets/img/45.jpg" class="w-100" alt="">
                </div>
            </div>
            <div class="row row-grid align-items-center mb-5 mb-lg-7">
                <div class="col-12 col-lg-5 order-lg-2">
                    <h2 class="mb-4">Driver Centric Business</h2>
                    <p>Zero commissions, payment within 12 hours, and a minimal flat fee are just a few of the benefits of being a CHOIS Partner.</p>
                </div>
                <div class="col-12 col-lg-6 mr-lg-auto">
                    <img src="./assets/img/82.jpg" class="w-100" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 mb-4">
                    <div class="card border-light p-4">
                        <div class="card-body">
                            <h2 class="display-2 mb-2">0%</h2>
                            <span>Drivers pay Zero percent commision for your rides.</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4">
                    <div class="card border-light p-4">
                        <div class="card-body">
                            <h2 class="display-2 mb-2">24/7</h2>
                            <span> Our support team is a quick chat or email away.</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4">
                    <div class="card border-light p-4">
                        <div class="card-body">
                            <h2 class="display-2 mb-2">220k+</h2>
                            <span>Happy customers and counting</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="section bg-soft" id="download">
        <figure class="position-absolute top-0 left-0 w-100 d-none d-md-block mt-n3">
            <svg class="fill-soft" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 43.4" style="enable-background:new 0 0 1920 43.4;" xml:space="preserve">
                <path d="M0,23.3c0,0,405.1-43.5,697.6,0c316.5,1.5,108.9-2.6,480.4-14.1c0,0,139-12.2,458.7,14.3 c0,0,67.8,19.2,283.3-22.7v35.1H0V23.3z"></path>
            </svg>
        </figure>
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-12 col-lg-6">
                    <span class="h5 text-muted mb-2 d-block">Download App</span>
                    <h2 class="display-3 mb-4">Go Surge Free</h2>
                    <p class="lead text-muted">With CHOIS, you’ll never worry about surge pricing again. Be it heavy rain or traffic, we offer predictable pricing. Go Surge Free.</p>
                    <div class="mt-4 mt-lg-5">
                        <!--  <a href="#" class="btn btn-dark btn-download-app mb-xl-0 mr-2 mr-md-3">
                                <span class="d-flex align-items-center">
                                    <span class="icon icon-brand mr-2 mr-md-3"><span class="fab fa-apple"></span></span>
                                    <span class="d-inline-block text-left">
                                        <small class="font-weight-normal d-none d-md-block">Available on</small> App Store 
                                    </span> 
                                </span>
                            </a> -->
                        <a href="https://play.google.com/store/apps/details?id=com.rideplus.customer" target="_blank" class="btn btn-dark btn-download-app">
                            <span class="d-flex align-items-center">
                                <span class="icon icon-brand mr-2 mr-md-3"><span class="fab fa-google-play"></span></span>
                                <span class="d-inline-block text-left">
                                    <small class="font-weight-normal d-none d-md-block">Available on</small> Google Play
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-lg-5 ml-lg-auto">
                    <img class="d-none d-lg-inline-block" src="./assets/img/download.png" alt="Mobile App Illustration">
                </div>
            </div>
        </div>
    </div>
</main>
@endsection