@extends('layout.app') @section('content')
<main>
    <!-- Hero -->
    <section class="section sonew-header">
        <div class="sonew-hero sonew-hero-background text-white">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 col-sm-12 text-left p-lg-7 p-5">
                    <h1 class="display-2 font-weight-bolder mb-4">Contact us</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="border-bottom border-gray-300 py-4" id="info">
        <div class="container">
            <div class="row">

                <div class="col-12 col-md-6 text-center">

                    <!-- Heading -->
                    <h6 class="text-uppercase text-gray-700 mb-1">
                        Email us
                    </h6>

                    <!-- Link -->
                    <a href="mailto:support@chois.in" class="h4">
                        support@chois.in
                    </a>

                </div>
                <div class="col-12 col-md-6 text-center">

                    <!-- Heading -->
                    <h6 class="text-uppercase text-gray-700 mb-1">
                        Office Address
                    </h6>

                    <!-- Link -->
                    <a href="#" class="h6">
                        #59 SRT Prakash Nagar, Begumpet, Hyderabad - 500016
                    </a>

                </div>
            </div> <!-- / .row -->
        </div>
    </section>



    <div class="section bg-soft" id="download">
        <figure class="position-absolute top-0 left-0 w-100 d-none d-md-block mt-n3">
            <svg class="fill-soft" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 43.4" style="enable-background:new 0 0 1920 43.4;" xml:space="preserve">
                <path d="M0,23.3c0,0,405.1-43.5,697.6,0c316.5,1.5,108.9-2.6,480.4-14.1c0,0,139-12.2,458.7,14.3 c0,0,67.8,19.2,283.3-22.7v35.1H0V23.3z"></path>
            </svg>
        </figure>
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-12 col-lg-6">
                    <span class="h5 text-muted mb-2 d-block">Download App</span>
                    <h2 class="display-3 mb-4">Go Surge Free</h2>
                    <p class="lead text-muted">With CHOIS, you’ll never worry about surge pricing again. Be it heavy rain or traffic, we offer predictable pricing. Go Surge Free.</p>
                    <div class="mt-4 mt-lg-5">
                        <!--  <a href="#" class="btn btn-dark btn-download-app mb-xl-0 mr-2 mr-md-3">
                                <span class="d-flex align-items-center">
                                    <span class="icon icon-brand mr-2 mr-md-3"><span class="fab fa-apple"></span></span>
                                    <span class="d-inline-block text-left">
                                        <small class="font-weight-normal d-none d-md-block">Available on</small> App Store 
                                    </span> 
                                </span>
                            </a> -->
                        <a href="https://play.google.com/store/apps/details?id=com.rideplus.partner" target="_blank" class="btn btn-dark btn-download-app">
                            <span class="d-flex align-items-center">
                                <span class="icon icon-brand mr-2 mr-md-3"><span class="fab fa-google-play"></span></span>
                                <span class="d-inline-block text-left">
                                    <small class="font-weight-normal d-none d-md-block">Available on</small> Google Play
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-lg-5 ml-lg-auto">
                    <img class="d-none d-lg-inline-block" src="./assets/img/download.png" alt="Mobile App Illustration">
                </div>
            </div>
        </div>
    </div>
</main>
@endsection