@extends('layout.app') @section('content')
<main>
    <!-- Hero -->

    <section class="section sonew-header">
        <div class="sonew-hero sonew-hero-background text-white">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 col-sm-12 text-left p-lg-7 p-5">
                    <h1 class="display-2 font-weight-bolder mb-4">Privacy Policy</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-lg py-0">
        <div class="container">
            <div class="row">
                <div>
                    <div id="page_1">
                        <div id="id1_1">
                            <p class="p0 ft0">CHOIS</p>
                            <p class="p1 ft0">Privacy Policy</p>
                            <p class="p2 ft0">Privacy Policy</p>
                            <p class="p3 ft0">Last updated: March 26, 2021</p>
                            <p class="p4 ft1">
                                This Privacy Policy describes Our policies and procedures on
                                the collection, use and disclosure of Your information when
                                You use the Service and tells You about Your privacy rights
                                and how the law protects You.
                            </p>
                            <p class="p5 ft1">
                                We use Your Personal data to provide and improve the
                                Service. By using the Service, you agree to the collection
                                and use of information in accordance with this Privacy
                                Policy. This Privacy Policy has been created with the help
                                of the Privacy Policy Generator.
                            </p>
                            <p class="p6 ft0">Interpretation and Definitions</p>
                            <p class="p7 ft0">Interpretation</p>
                            <p class="p8 ft1">
                                The words of which the initial letter is capitalized have
                                meanings defined under the following conditions. The
                                following definitions shall have the same meaning regardless
                                of whether they appear in singular or in plural.
                            </p>
                            <p class="p6 ft0">Definitions</p>
                            <p class="p3 ft0">For the purposes of this Privacy Policy:</p>
                            <p class="p9 ft1">
                                Account means a unique account created for You to access our
                                Service or parts of our Service.
                            </p>
                            <p class="p10 ft2">
                                Affiliate means an entity that controls, is controlled by or
                                is under common control with a party, where "control" means
                                ownership of 50% or more of the shares, equity interest or
                                other securities entitled to vote for election of directors
                                or other managing authority Application means the software
                                program provided by the Company downloaded by You on any
                                electronic device, named "CHOIS"
                            </p>
                            <p class="p11 ft1">
                                Company (referred to as either "the Company", "We", "Us" or
                                "Our" in this Agreement) refers to RidePlus Technologies
                                Private Limited, #59 Srt Prakash nagar, Begumpet, Hyderabad,
                                Telangana 500016.
                            </p>
                            <p class="p12 ft2">
                                Cookies are small files that are placed on Your computer,
                                mobile device or any other device by a website, containing
                                the details of Your browsing history on that website among
                                its many uses.
                            </p>
                            <p class="p0 ft0">Country refers to: Telangana, India</p>
                            <p class="p13 ft1">
                                Device means any device that can access the Service such as
                                a computer, a cell phone or a digital tablet
                            </p>
                            <p class="p14 ft2">
                                Personal Data is any information that relates to an
                                identified or identifiable individual. Service refers to the
                                Application or the Website or both.
                            </p>
                            <p class="p15 ft1">
                                Service Provider means any natural or legal person who
                                processes the data on behalf of the Company. It refers to
                                <nobr>third-party</nobr> companies or individuals employed
                                by the Company to
                            </p>
                        </div>
                        <div id="id1_2"></div>
                    </div>
                    <div id="page_2">
                        <div id="id2_1">
                            <p class="p16 ft2">
                                facilitate the Service, to provide the Service on behalf of
                                the Company, to perform services related to the Service or
                                to assist the Company in analysing how the Service is used.
                                <nobr>Third-party</nobr> Social Media Service refers to any
                                website or any social network website through which a User
                                can log in or create an account to use the Service.
                            </p>
                            <p class="p17 ft1">
                                Usage Data refers to data collected automatically, either
                                generated by the use of the Service or from the Service
                                infrastructure itself (for example, the duration of a page
                                visit).
                            </p>
                            <p class="p0 ft0">
                                Website refers to CHOIS, accessible from https://chois/
                            </p>
                            <p class="p18 ft1">
                                You mean the individual accessing or using the Service, or
                                the company, or other legal entity on behalf of which such
                                individual is accessing or using the Service, as applicable
                                Collecting and Using Your Personal Data
                            </p>
                            <p class="p6 ft0">Types of Data Collected</p>
                            <p class="p19 ft4">
                                While using Our Service, we may ask You to provide Us with
                                certain personally identifiable information that can be used
                                to contact or identify You. Personally, identifiable
                                information may include, but is not limited to:
                            </p>
                            <p class="p20 ft0">Email address</p>
                            <p class="p21 ft0">First name and last name</p>
                            <p class="p1 ft0">Phone number</p>
                            <p class="p21 ft0">
                                Address, State, Province, ZIP/Postal code, City
                            </p>
                            <p class="p1 ft0">Usage Data</p>
                            <p class="p2 ft0">
                                Usage Data is collected automatically when using the
                                Service.
                            </p>
                            <p class="p22 ft1">
                                Usage Data may include information such as Your Device's
                                Internet Protocol address (e.g., IP address), browser type,
                                browser version, the pages of our Service that You visit,
                                the time and date of Your visit, the time spent on those
                                pages, unique device identifiers and other diagnostic data.
                            </p>
                            <p class="p23 ft1">
                                When You access the Service by or through a mobile device,
                                we may collect certain information automatically, including,
                                but not limited to, the type of mobile device You use, your
                                mobile device unique ID, the IP address of Your mobile
                                device, your mobile operating system, the type of mobile
                                Internet browser You use, unique device identifiers and
                                other diagnostic data.
                            </p>
                            <p class="p24 ft1">
                                We may also collect information that Your browser sends
                                whenever You visit our Service or when You access the
                                Service by or through a mobile device.
                            </p>
                            <p class="p6 ft0">
                                Information from <nobr>Third-Party</nobr> Social Media
                                Services
                            </p>
                            <p class="p25 ft1">
                                The Company allows You to create an account and log in to
                                use the Service through the following
                                <nobr>Third-party</nobr> Social Media Services:
                            </p>
                            <p class="p6 ft0">Google</p>
                            <p class="p1 ft0">Facebook</p>
                            <p class="p21 ft0">Twitter</p>
                        </div>
                        <div id="id2_2"></div>
                    </div>
                    <div id="page_3">
                        <div id="id3_1">
                            <p class="p0 ft0">Instagram</p>
                            <p class="p1 ft0">LinkedIn</p>
                            <p class="p17 ft1">
                                If You decide to register through or otherwise grant us
                                access to a <nobr>Third-Party</nobr> Social Media Service,
                                we may collect Personal data that is already associated with
                                Your <nobr>Third-party</nobr> Social Media Service's
                                account, such as Your name, your email address, your
                                activities or Your contact list associated with that
                                account.
                            </p>
                            <p class="p26 ft1">
                                You may also have the option of sharing additional
                                information with the Company through Your
                                <nobr>Third-Party</nobr> Social Media Service's account. If
                                You choose to provide such information and Personal Data,
                                during registration or otherwise, you are giving the Company
                                permission to use, share, and store it in a manner
                                consistent with this Privacy Policy.
                            </p>
                            <p class="p27 ft0">
                                Information Collected while Using the Application
                            </p>
                            <p class="p28 ft1">
                                While using Our Application, in order to provide features of
                                Our Application, we may collect, with Your prior permission:
                            </p>
                            <p class="p6 ft0">Information regarding your location</p>
                            <p class="p21 ft0">
                                Information from your Device's phone book (contacts list)
                            </p>
                            <p class="p1 ft0">
                                Pictures and other information from your Device's camera and
                                photo library
                            </p>
                            <p class="p29 ft1">
                                We use this information to provide features of Our Service,
                                to improve and customize Our Service. The information may be
                                uploaded to the Company's servers and/or a Service
                                Provider's server or it may be simply stored on Your device.
                            </p>
                            <p class="p30 ft1">
                                You can enable or disable access to this information at any
                                time, through Your Device settings.
                            </p>
                            <p class="p6 ft0">Tracking Technologies and Cookies</p>
                            <p class="p31 ft1">
                                We use Cookies and similar tracking technologies to track
                                the activity on Our Service and store certain information.
                                Tracking technologies used are beacons, tags, and scripts to
                                collect and track information and to improve and analyse Our
                                Service. The technologies We use may include:
                            </p>
                            <p class="p32 ft2">
                                Cookies or Browser Cookies. A cookie is a small file placed
                                on Your Device. You can instruct Your browser to refuse all
                                Cookies or to indicate when a Cookie is being sent. However,
                                if You do not accept Cookies, you may not be able to use
                                some parts of our Service. Unless you have adjusted Your
                                browser setting so that it will refuse Cookies, our Service
                                may use Cookies.
                            </p>
                            <p class="p33 ft2">
                                Flash Cookies. Certain features of our Service may use local
                                stored objects (or Flash Cookies) to collect and store
                                information about Your preferences or Your activity on our
                                Service. Flash Cookies are not managed by the same browser
                                settings as those used for Browser Cookies.
                            </p>
                            <p class="p34 ft1">
                                Web Beacons. Certain sections of our Service and our emails
                                may contain small electronic files known as web beacons
                                (also referred to as clear gifs, pixel tags, and
                                <nobr>single-pixel</nobr> gifs) that permit the Company, for
                                example, to count users who have visited those pages or
                                opened an email and for other related website statistics
                                (for example, recording the popularity of a certain section
                                and verifying system and server integrity).
                            </p>
                        </div>
                        <div id="id3_2"></div>
                    </div>
                    <div id="page_4">
                        <div id="id4_1">
                            <p class="p35 ft4">
                                Cookies can be "Persistent" or "Session" Cookies. Persistent
                                Cookies remain on Your personal computer or mobile device
                                when You go offline, while Session Cookies are deleted as
                                soon as You close Your web browser. Learn more about
                                cookies: What Are Cookies?
                            </p>
                            <p class="p20 ft0">
                                We use both Session and Persistent Cookies for the purposes
                                set out below:
                            </p>
                            <p class="p3 ft0">Necessary / Essential Cookies</p>
                            <p class="p36 ft0">Type: Session Cookies</p>
                            <p class="p37 ft0">Administered by: Us</p>
                            <p class="p22 ft1">
                                Purpose: These Cookies are essential to provide You with
                                services available through the Website and to enable You to
                                use some of its features. They help to authenticate users
                                and prevent fraudulent use of user accounts. Without these
                                Cookies, the services that You have asked for cannot be
                                provided, and We only use these Cookies to provide You with
                                those services.
                            </p>
                            <p class="p38 ft0">
                                Cookies Policy / Notice Acceptance Cookies
                            </p>
                            <p class="p36 ft0">Type: Persistent Cookies</p>
                            <p class="p39 ft0">Administered by: Us</p>
                            <p class="p3 ft0">
                                Purpose: These Cookies identify if users have accepted the
                                use of cookies on the Website
                            </p>
                            <p class="p3 ft0">Functionality Cookies</p>
                            <p class="p36 ft0">Type: Persistent Cookies</p>
                            <p class="p40 ft0">Administered by: Us</p>
                            <p class="p41 ft1">
                                Purpose: These Cookies allow us to remember choices You make
                                when You use the Website, such as remembering your login
                                details or language preference. The purpose of these Cookies
                                is to provide You with a more personal experience and to
                                avoid You having to <nobr>re-enter</nobr> your preferences
                                every time You use the Website.
                            </p>
                            <p class="p26 ft1">
                                For more information about the cookies, we use and your
                                choices regarding cookies, please visit our Cookies Policy
                                or the Cookies section of our Privacy Policy
                            </p>
                            <p class="p6 ft0">Use of Your Personal Data</p>
                            <p class="p3 ft0">
                                The Company may use Personal Data for the following
                                purposes:
                            </p>
                            <p class="p42 ft2">
                                To provide and maintain our Service, including to monitor
                                the usage of our Service. To manage Your Account: to manage
                                Your registration as a user of the Service. The Personal
                                Data You provide can give You access to different
                                functionalities of the Service that are available to You as
                                a registered user.
                            </p>
                            <p class="p43 ft1">
                                For the performance of a contract: the development,
                                compliance and undertaking of the purchase contract for the
                                products, items or services You have purchased or of any
                                other contract with Us through the Service.
                            </p>
                        </div>
                        <div id="id4_2"></div>
                    </div>
                    <div id="page_5">
                        <div id="id5_1">
                            <p class="p44 ft2">
                                To contact You: To contact You by email, telephone calls,
                                SMS, or other equivalent forms of electronic communication,
                                such as a mobile application's push notifications regarding
                                updates or informative communications related to the
                                functionalities, products or contracted services, including
                                the security updates, when necessary or reasonable for their
                                implementation.
                            </p>
                            <p class="p45 ft1">
                                To provide You with news, special offers and general
                                information about other goods, services and events which we
                                offer that are similar to those that you have already
                                purchased or enquired about unless You have opted not to
                                receive such information.
                            </p>
                            <p class="p0 ft0">
                                To manage Your requests: To attend and manage Your requests
                                to Us.
                            </p>
                            <p class="p46 ft2">
                                For business transfers: We may use Your information to
                                evaluate or conduct a merger, divestiture, restructuring,
                                reorganization, dissolution, or other sale or transfer of
                                some or all of Our assets, whether as a going concern or as
                                part of bankruptcy, liquidation, or similar proceeding, in
                                which Personal Data held by Us about our Service users is
                                among the assets transferred.
                            </p>
                            <p class="p47 ft2">
                                For business transfers: We may use Your information to
                                evaluate or conduct a merger, divestiture, restructuring,
                                reorganization, dissolution, or other sale or transfer of
                                some or all of Our assets, whether as a going concern or as
                                part of bankruptcy, liquidation, or similar proceeding, in
                                which Personal Data held by Us about our Service users is
                                among the assets transferred.
                            </p>
                            <p class="p48 ft1">
                                With business partners: We may share Your information with
                                Our business partners to offer You certain products,
                                services or promotions.
                            </p>
                            <p class="p49 ft2">
                                With other users: when You share personal information or
                                otherwise interact in the public areas with other users,
                                such information may be viewed by all users and may be
                                publicly distributed outside. If You interact with other
                                users or register through a <nobr>Third-Party</nobr> Social
                                Media Service, your contacts on the
                                <nobr>Third-Party</nobr> Social Media Service may see Your
                                name, profile, pictures and description of Your activity.
                                Similarly, other users will be able to view descriptions of
                                Your activity, communicate with You and view Your profile.
                            </p>
                            <p class="p50 ft1">
                                With Your consent: We may disclose Your personal information
                                for any other purpose with Your consent.
                            </p>
                            <p class="p0 ft0">Retention of Your Personal Data</p>
                            <p class="p51 ft1">
                                The Company will retain Your Personal Data only for as long
                                as is necessary for the purposes set out in this Privacy
                                Policy. We will retain and use Your Personal Data to the
                                extent necessary to comply with our legal obligations (for
                                example, if we are required to retain your data to comply
                                with applicable laws), resolve disputes, and enforce our
                                legal agreements and policies.
                            </p>
                            <p class="p52 ft1">
                                The Company will also retain Usage Data for internal
                                analysis purposes. Usage Data is generally retained for a
                                shorter period of time, except when this data is used to
                                strengthen the security or to improve the functionality of
                                Our Service, or We are legally obligated to retain this data
                                for longer time periods.
                            </p>
                            <p class="p27 ft0">Transfer of Your Personal Data</p>
                            <p class="p53 ft1">
                                Your information, including Personal Data, is processed at
                                the Company's operating offices and in any other places
                                where the parties involved in the processing are located. It
                                means that this information may be transferred to — and
                                maintained on — computers located
                            </p>
                        </div>
                        <div id="id5_2"></div>
                    </div>
                    <div id="page_6">
                        <div id="id6_1">
                            <p class="p54 ft1">
                                outside of Your state, province, country or other
                                governmental jurisdiction where the data protection laws may
                                differ than those from Your jurisdiction.
                            </p>
                            <p class="p55 ft1">
                                Your consent to this Privacy Policy followed by Your
                                submission of such information represents Your agreement to
                                that transfer.
                            </p>
                            <p class="p56 ft1">
                                The Company will take all steps reasonably necessary to
                                ensure that Your data is treated securely and in accordance
                                with this Privacy Policy and no transfer of Your Personal
                                Data will take place to an organization or a country unless
                                there are adequate controls in place including the security
                                of Your data and other personal information.
                            </p>
                            <p class="p27 ft0">Disclosure of Your Personal Data</p>
                            <p class="p7 ft0">Business Transactions</p>
                            <p class="p57 ft1">
                                If the Company is involved in a merger, acquisition or asset
                                sale, Your Personal Data may be transferred. We will provide
                                notice before Your Personal Data is transferred and becomes
                                subject to a different Privacy Policy.
                            </p>
                            <p class="p6 ft0">Law enforcement</p>
                            <p class="p58 ft1">
                                Under certain circumstances, the Company may be required to
                                disclose Your Personal Data if required to do so by law or
                                in response to valid requests by public authorities (e.g., a
                                court or a government agency).
                            </p>
                            <p class="p27 ft0">Other legal requirements</p>
                            <p class="p59 ft1">
                                Other legal requirements The Company may disclose Your
                                Personal Data in the good faith belief that such action is
                                necessary to:
                            </p>
                            <p class="p6 ft0">Comply with a legal obligation</p>
                            <p class="p1 ft0">
                                Protect and defend the rights or property of the Company
                            </p>
                            <p class="p60 ft1">
                                Prevent or investigate possible wrongdoing in connection
                                with the Service Protect the personal safety of Users of the
                                Service or the public Protect against legal liability
                            </p>
                            <p class="p0 ft0">Security of Your Personal Data</p>
                            <p class="p61 ft1">
                                The security of Your Personal Data is important to Us, but
                                remember that no method of transmission over the Internet,
                                or method of electronic storage is 100% secure. While We
                                strive to use commercially acceptable means to protect Your
                                Personal Data, we cannot guarantee its absolute security.
                            </p>
                            <p class="p27 ft0">Children's Privacy</p>
                            <p class="p62 ft1">
                                Our Service does not address anyone under the age of 18. We
                                do not knowingly collect personally identifiable information
                                from anyone under the age of 18. If You are a parent or
                                guardian and You are aware that Your child has provided Us
                                with Personal Data, please contact Us. If We become aware
                                that We have collected Personal Data from anyone under
                            </p>
                        </div>
                        <div id="id6_2"></div>
                    </div>
                    <div id="page_7">
                        <div id="id7_1">
                            <p class="p63 ft1">
                                the age of 13 without verification of parental consent, we
                                take steps to remove that information from Our servers.
                            </p>
                            <p class="p64 ft0">Payment Processing</p>
                            <p class="p65 ft1">
                                Payment details you provide will be encrypted using secure
                                sockets layer (SSL) technology before they are
                            </p>
                            <p class="p16 ft2">
                                submitted to us over the internet. Payments made on the
                                Website/App are made through our payment
                            </p>
                            <p class="p66 ft1">
                                gateway providers. You will be providing credit or debit
                                card information directly to our payment gateway
                            </p>
                            <p class="p67 ft2">
                                providers, which operate secure servers to process payment
                                details, encrypting your credit/debit card
                            </p>
                            <p class="p68 ft1">
                                information and authorising payment. Information which you
                                supply to our payment service providers is
                            </p>
                            <p class="p16 ft2">
                                not within our control and is subject to the respective
                                providers’ own privacy policy and terms and
                            </p>
                            <p class="p0 ft0">conditions.</p>
                            <p class="p69 ft0">How we protect your information</p>
                            <p class="p70 ft1">
                                All information you provide to us is stored on our secure
                                servers. Any payment transactions will be
                            </p>
                            <p class="p71 ft2">
                                encrypted using SSL technology. Where we have given you (or
                                where you have chosen) a password which
                            </p>
                            <p class="p72 ft1">
                                enables you to access certain parts of the Services, you are
                                responsible for keeping this password
                            </p>
                            <p class="p0 ft0">
                                confidential. We ask you not to share a password with
                                anyone.
                            </p>
                            <p class="p73 ft1">
                                Unfortunately, the transmission of information via the
                                internet is not completely secure. Although we will
                            </p>
                            <p class="p72 ft2">
                                do our best to protect your personal data, we cannot
                                guarantee the security of your data transmitted to the
                            </p>
                            <p class="p44 ft2">
                                services; any transmission is at your own risk. Once we have
                                received your information, we will use strict
                            </p>
                            <p class="p0 ft0">
                                procedures and security features to try to prevent
                                unauthorised access.
                            </p>
                            <p class="p45 ft1">
                                Our Site may, from time to time, contain links to external
                                sites. We are not responsible for the privacy
                            </p>
                            <p class="p74 ft2">
                                policies or the content of such sites. How long we keep your
                                information
                            </p>
                            <p class="p10 ft1">
                                We retain personal data for (i) as long as you have an
                                account with us in order to meet our contractual
                            </p>
                            <p class="p75 ft2">
                                obligations to you, and (ii) for six years after that to
                                identify any issues and resolve any legal proceedings.
                            </p>
                            <p class="p76 ft1">
                                We may retain information for any other legal or regulatory
                                requirement. During the period of retention,
                            </p>
                            <p class="p77 ft2">
                                whether or not you use the App/ Website or access the
                                Services, we may use your information for purposes
                            </p>
                            <p class="p78 ft1">
                                of taxation, litigation or resolution of any claims,
                                insurance, safety and security, fraud management orHow we
                                protect your information
                            </p>
                        </div>
                    </div>
                    <div id="page_8">
                        <div id="id8_1">
                            <p class="p79 ft1">
                                All information you provide to us is stored on our secure
                                servers. Any payment transactions will be
                            </p>
                            <p class="p71 ft2">
                                encrypted using SSL technology. Where we have given you (or
                                where you have chosen) a password which
                            </p>
                            <p class="p72 ft1">
                                enables you to access certain parts of the Services, you are
                                responsible for keeping this password
                            </p>
                            <p class="p0 ft0">
                                confidential. We ask you not to share a password with
                                anyone.
                            </p>
                            <p class="p73 ft1">
                                Unfortunately, the transmission of information via the
                                internet is not completely secure. Although we will
                            </p>
                            <p class="p72 ft2">
                                do our best to protect your personal data, we cannot
                                guarantee the security of your data transmitted to the
                            </p>
                            <p class="p44 ft1">
                                services; any transmission is at your own risk. Once we have
                                received your information, we will use strict
                            </p>
                            <p class="p0 ft0">
                                procedures and security features to try to prevent
                                unauthorised access.
                            </p>
                            <p class="p80 ft1">
                                Our Site may, from time to time, contain links to external
                                sites. We are not responsible for the privacy
                            </p>
                            <p class="p74 ft2">
                                policies or the content of such sites. How long we keep your
                                information
                            </p>
                            <p class="p10 ft1">
                                We retain personal data for (i) as long as you have an
                                account with us in order to meet our contractual
                            </p>
                            <p class="p75 ft2">
                                obligations to you, and (ii) for six years after that to
                                identify any issues and resolve any legal proceedings.
                            </p>
                            <p class="p76 ft2">
                                We may retain information for any other legal or regulatory
                                requirement. During the period of retention,
                            </p>
                            <p class="p77 ft1">
                                whether or not you use the App/ Website or access the
                                Services, we may use your information for purposes
                            </p>
                            <p class="p78 ft1">
                                of taxation, litigation or resolution of any claims,
                                insurance, safety and security, fraud management
                                oranonymised information retained or used for these
                                purposes.
                            </p>
                            <p class="p27 ft0">Links to Other Websites</p>
                            <p class="p81 ft1">
                                Our Service may contain links to other websites that are not
                                operated by Us. If You click on a
                                <nobr>third-party</nobr> link, you will be directed to that
                                third party's site. We strongly advise You to review the
                                Privacy Policy of every site You visit.
                            </p>
                            <p class="p82 ft1">
                                We have no control over and assume no responsibility for the
                                content, privacy policies or practices of any
                                <nobr>third-party</nobr> sites or services.
                            </p>
                            <p class="p6 ft0">Changes to this Privacy Policy</p>
                            <p class="p83 ft1">
                                We may update Our Privacy Policy from time to time. We will
                                notify You of any changes by posting the new Privacy Policy
                                on this page.
                            </p>
                            <p class="p84 ft1">
                                We will let You know via email and/or a prominent notice on
                                Our Service, prior to the change becoming effective and
                                update the "Last updated" date at the top of this Privacy
                                Policy.
                            </p>
                            <p class="p85 ft1">
                                You are advised to review this Privacy Policy periodically
                                for any changes. Changes to this Privacy Policy are
                                effective when they are posted on this page.
                            </p>
                        </div>
                    </div>
                    <div id="page_9">
                        <div id="id9_1">
                            <p class="p0 ft0">
                                Stay <nobr>up-to-date</nobr> on all our exciting offers and
                                latest happenings by connecting with us online.
                            </p>
                            <p class="p21 ft0">
                                <span class="ft0">•</span><span class="ft5">Follow us on Twitter - twitter.com/Choiscabs</span>
                            </p>
                            <p class="p1 ft0">
                                <span class="ft0">•</span><span class="ft5">Like our Facebook page - facebook.com/Choiscabs</span>
                            </p>
                            <p class="p21 ft0">
                                <span class="ft0">•</span><span class="ft5">Follow us on Instagram - instagram.com/Choiscabs</span>
                            </p>
                            <p class="p3 ft0">Contact Us</p>
                            <p class="p3 ft0">
                                If you have any questions about this Privacy Policy, you can
                                contact us:
                            </p>
                            <p class="p7 ft0">By email: info@chois.in</p>
                            <p class="p1 ft0">
                                By visiting this page on our website: https://chois.in/
                            </p>
                            <p class="p21 ft0">
                                Copyright © 2021RidePlus Technologies Private Limited - All
                                Rights Reserved.
                            </p>
                        </div>
                        <div id="id9_2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection