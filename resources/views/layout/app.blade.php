<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Primary Meta Tags -->
    <title>CHOIS - Go Surge Free</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="title" content="Chois go surge free">
    <meta name="author" content="sonew">
    <meta name="description" content="Chois cabs go sruge free">
    <meta name="keywords" content="chois, chois cabs, surge free, surge charges, taxi, cab booking" />

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://chois.in">
    <meta property="og:title" content="Chois - Go Surge Free">
    <meta property="og:description" content="Chois cabs makes your life easy by allowing you to tavel without worrying about surges charges. ">
    <meta property="og:image" content="https://chois.in/thumbnail.jpg">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://chois.in">
    <meta property="twitter:title" content="Chois - Go Surge Free">
    <meta property="twitter:description" content="Chois cabs makes your life easy by allowing you to tavel without worrying about surges charges.">
    <meta property="twitter:image" content="https://chois.in/thumbnail.jpg">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/assets/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="/assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- Fontawesome -->
    <link type="text/css" href="/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Swipe CSS -->
    <link type="text/css" href="/css/swipe.css" rel="stylesheet">
    <link type="text/css" href="/css/sonew.css" rel="stylesheet">

    <!-- NOTICE: You can use the _analytics.html partial to include production code specific code & trackers -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-201272363-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-201272363-1');
    </script>

</head>

<body>
    @include('templates.header')
    @yield('content')
    @include('templates.footer')
</body>

</html>