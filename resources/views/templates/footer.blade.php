<footer class="footer py-5 pt-lg-6">
    <div class="sticky-right">
        <a href="#home" class="icon icon-primary icon-md btn btn-icon-only btn-white border border-soft shadow-soft animate-up-3">
            <span class="fas fa-chevron-up"></span>
        </a>
    </div>
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-3">
                <p>Stay up-to-date on all our exciting offers and latest happenings by connecting with us online.</p>
                <ul class="social-buttons mb-5 mb-lg-0">
                    <li>
                        <a href="https://twitter.com/choiscabs" aria-label="twitter social link" class="icon icon-md icon-twitter mr-3">
                            <span class="fab fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/choiscabs/" class="icon icon-md icon-facebook mr-3" aria-label="facebook social link">
                            <span class="fab fa-facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://instagram.com/choiscabs" aria-label="Instagram social link" class="icon icon-md icon-instagram mr-3">
                            <span class="fab fa-instagram"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-6 col-md-3 mb-5 mb-lg-0">
                <span class="h5">Chois</span>
                <ul class="footer-links mt-2">
                    <li><a href="{{route('refund')}}">Refund Policy</a></li>
                    <li><a href="{{route('privacy-policy')}}">Privacy Policy</a></li>
                    <li><a href="{{route('terms-and-conditions')}}">Terms and Conditions</a></li>
                    <li><a href="{{route('contact-us')}}">Contact us</a></li>

                </ul>
            </div>
            <div class="col-12 col-md-4 mb-5 mb-lg-0">
                <span class="h5 mb-3 d-block">Subscribe</span>
                <form action="#">
                    <div class="form-row mb-2">
                        <div class="col-12">
                            <input type="email" class="form-control mb-2" placeholder="example@company.com" name="email" aria-label="Subscribe form" required>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-dark shadow-soft btn-block" data-loading-text="Sending">
                                <span>Subscribe</span>
                            </button>
                        </div>
                    </div>
                </form>
                <p class="text-muted font-small m-0">No spam. Pinky swear.</p>
            </div>
        </div>
        <hr class="bg-light my-2">
        <div class="row pt-2 pt-lg-5">
            <div class="col mb-md-0">
                <div class="d-flex text-center justify-content-center align-items-center" role="contentinfo">
                    <p class="font-weight-normal font-small mb-0">Copyright © chois 2019-<span class="current-year">{{date('Y')}}</span>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Core -->
<script src="/vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/vendor/headroom.js/dist/headroom.min.js"></script>

<!-- Vendor JS -->
<script src="/vendor/onscreen/dist/on-screen.umd.min.js"></script>
<script src="/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>



<!-- Swipe JS -->
<script src="/assets/js/swipe.js"></script>